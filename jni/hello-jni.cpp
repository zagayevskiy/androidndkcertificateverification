/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#include <string.h>
#include <jni.h>

/* This is a trivial JNI example where we use a native method
 * to return a new VM String. See the corresponding Java source
 * file located at:
 *
 *   apps/samples/hello-jni/project/src/com/example/hellojni/HelloJni.java
 */
 #define ERROR 0
 
 
#include <android/log.h>

#define  LOG_TAG    "native log"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

 
 extern "C"{
	 
char*getCertificate(JNIEnv *env, jobject obj, jobject cnt) {
	 jclass cls = env->GetObjectClass(cnt);
    jmethodID mid = env->GetMethodID(cls, "getPackageManager",
            "()Landroid/content/pm/PackageManager;");
    jmethodID pnid = env->GetMethodID(cls, "getPackageName",
            "()Ljava/lang/String;");
    if (mid == 0 || pnid == 0) {
        return ERROR;
    }

    jobject pacMan_o = env->CallObjectMethod(cnt, mid);
    jclass pacMan = env->GetObjectClass(pacMan_o);
    jstring packName = (jstring) env->CallObjectMethod(cnt, pnid);

    /*flags = PackageManager.GET_SIGNATURES*/
    int flags = 0x40;
    mid = env->GetMethodID(pacMan, "getPackageInfo",
            "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    if (mid == 0) {
        return ERROR;
    }
    jobject pack_inf_o = (jobject) env->CallObjectMethod(pacMan_o, mid,
            packName, flags);

    jclass packinf = env->GetObjectClass(pack_inf_o);
    jfieldID fid;
    fid = env->GetFieldID(packinf, "signatures",
            "[Landroid/content/pm/Signature;");
    jobjectArray signatures = (jobjectArray) env->GetObjectField(pack_inf_o,
            fid);
    jobject signature0 = env->GetObjectArrayElement(signatures, 0);
    mid = env->GetMethodID(env->GetObjectClass(signature0), "toByteArray",
            "()[B");
    jbyteArray cert = (jbyteArray) env->CallObjectMethod(signature0, mid);
    if (cert == 0) {
        return ERROR;
    }
    jclass BAIS = env->FindClass("java/io/ByteArrayInputStream");
    if (BAIS == 0) {
        return ERROR;
    }
    mid = env->GetMethodID(BAIS, "<init>", "([B)V");
    if (mid == 0) {
        return ERROR;
    }
    jobject input = env->NewObject(BAIS, mid, cert);

    jclass CF = env->FindClass("java/security/cert/CertificateFactory");
    mid = env->GetStaticMethodID(CF, "getInstance",
            "(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;");

    jstring X509 = env->NewStringUTF("X509");
    jobject cf = env->CallStaticObjectMethod(CF, mid, X509);
    if (cf == 0) {
        return ERROR;
    }
    //"java/security/cert/X509Certificate"
    mid = env->GetMethodID(CF, "generateCertificate",
            "(Ljava/io/InputStream;)Ljava/security/cert/Certificate;");
    if (mid == 0) {
        return ERROR;
    }
    jobject c = env->CallObjectMethod(cf, mid, input);
    if (c == 0) {
        return ERROR;
    }
    jclass X509Cert = env->FindClass("java/security/cert/X509Certificate");
    mid = env->GetMethodID(X509Cert, "getPublicKey",
            "()Ljava/security/PublicKey;");
    jobject pk = env->CallObjectMethod(c, mid);
    if (pk == 0) {
        return ERROR;
    }
    mid = env->GetMethodID(env->GetObjectClass(pk), "toString",
            "()Ljava/lang/String;");
    if (mid == 0) {
        return ERROR;
    }
    jstring all = (jstring) env->CallObjectMethod(pk, mid);
    const char * all_char = env->GetStringUTFChars(all, NULL);
    char * out = NULL;
    if (all_char != NULL) {
        char * startString = strstr(all_char, "modulus:");
        char * end = strstr(all_char, "public exponent");
        bool isJB = false;
        if (startString == NULL) {
            //4.1.x
            startString = strstr(all_char, "modulus=");
            end = strstr(all_char, ",publicExponent");
            isJB = true;
        }
        if (startString != NULL && end != NULL) {
            int len;
            if (isJB) {
                startString += strlen("modulus=");
                len = end - startString;
            } else {
                startString += strlen("modulus:");
                len = end - startString - 5; /* -5 for new lines*/
            }
            out = new char[len + 2];
            strncpy(out, startString, len);
            out[len] = '\0';
        }
    }

    env->ReleaseStringUTFChars(all, all_char);
	return out;
}
	 
jstring
Java_com_mycompany_myndkapp2_HelloJni_stringFromJNI( JNIEnv* env,
                                                  jobject obj, jobject cnt )
{
	char* out = getCertificate(env, obj, cnt);
	jstring result = env->NewStringUTF(out);
	delete[] out;
    return result;
}
const char* rsa = "c99a3bce5c1bf548d6d22b657f0b0c2b56820e4f8cb882461d5385d3bb30f8bb06cc666c3ef74078c253615e82224d2c22a9af505cd780230fa20d0f024cff86dd8ed99af8138e8311134d8ac1583b683185c38f25cb6c7bcb41bb68ac7fb484766414ebd347e2996c707c2b1093134159375b9cd4d42f26110c3e9c462ae209";// "PUT_YOUR_RSA_KEY_HERE";
jint verifyCertificate(JNIEnv *env, jobject obj, jobject cnt) {
   char* out = getCertificate(env, obj, cnt);
    char * is_found = strstr(out, rsa);
	LOGI(out);
	delete[] out;
    
    return is_found != NULL ? 1 :0;
}


JNIEXPORT void JNICALL Java_com_mycompany_myndkapp2_HelloJni_init(JNIEnv *env, jobject obj, jobject ctx) {
    LOGI(verifyCertificate(env, obj, ctx) ? "verified" : "not verified");
}


}
